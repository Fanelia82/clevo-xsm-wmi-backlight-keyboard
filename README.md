# Clevo xsm wmi backlight keyboard

A gnome shell extension backlight keyboard controller based on tuxedocomputers-clevo-xsm-wmi

**ATTENTION!!!!**
There is a change to the permissions of the configuration files that can generate a security problem. Use wisely and on your own risk!


More details on future.

**License**

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.