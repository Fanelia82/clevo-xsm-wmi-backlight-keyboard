### License
This program is free software;  you can redistribute it and/or modify
it under the terms of the  GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is  distributed in the hope that it  will be useful, but
WITHOUT  ANY   WARRANTY;  without   even  the  implied   warranty  of
MERCHANTABILITY  or FITNESS FOR  A PARTICULAR  PURPOSE.  See  the GNU
General Public License for more details.

You should  have received  a copy of  the GNU General  Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

# Install clevo-xsm-wmi

Install the modified Kernel module for keyboard backlighting of Clevo SM series notebooks.
(And several EM/ZM/DM series models)

Based upon tuxedo-wmi, created by TUXEDO Computers GmbH.
http://www.linux-onlineshop.de/forum/index.php?page=Thread&threadID=26

see clevoxsmwmi@fanelia82.org/clevo-xsm-wmi-mod/README.ME for instructions


# Install the shell extension

Copy the folder in

	~/.local/share/gnome-shell/extensions/
	
